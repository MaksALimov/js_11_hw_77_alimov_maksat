import {SEND_MESSAGE_FAILURE, SEND_MESSAGE_REQUEST, SEND_MESSAGE_SUCCESS} from "../actions/sendMessageActions";

const initialState = {
    error: null,
    loading: null,
};

const sendMessageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE_REQUEST:
            return {...state, loading: true};

        case SEND_MESSAGE_SUCCESS:
            return {...state, loading: false, error: false};

        case SEND_MESSAGE_FAILURE:
            return {...state, error: action.payload, loading: false};

        default:
            return state;
    }
};

export default sendMessageReducer;