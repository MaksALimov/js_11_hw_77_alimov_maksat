import {
    CHANGE_MODAL,
    GET_MESSAGES_FAILURE,
    GET_MESSAGES_REQUEST,
    GET_MESSAGES_SUCCESS
} from "../actions/messagesActions";

const initialState = {
    error: null,
    messages: [],
    loading: false,
    isOpenModal: false,
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MESSAGES_REQUEST:
            return {...state, loading: true};

        case GET_MESSAGES_SUCCESS:
            return {...state, loading: false, messages: action.payload};

        case GET_MESSAGES_FAILURE:
            return {...state, error: action.payload};

        case CHANGE_MODAL:
            return {...state, isOpenModal: action.payload};

        default:
            return state;
    }
};

export default messagesReducer;