import axios from "axios";
import {getMessages} from "./messagesActions";

export const SEND_MESSAGE_REQUEST = 'SEND_MESSAGE_REQUEST';
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAILURE = 'SEND_MESSAGE_FAILURE';

export const sendMessageRequest = () => ({type: SEND_MESSAGE_REQUEST});
export const sendMessageSuccess = () => ({type: SEND_MESSAGE_SUCCESS});
export const sendMessageFailure = error => ({type: SEND_MESSAGE_FAILURE, payload: error});

export const sendMessage = message => {
    return async dispatch => {
        try {
             dispatch(sendMessageRequest());
            await axios.post('http://127.0.0.1:8888/messages', message);
             dispatch(sendMessageSuccess());
             dispatch(getMessages());
        } catch (error) {
            dispatch(sendMessageFailure(error));
            //Генерирую исключение, чтобы модалка не закрылась
            throw Error
        }
    };
};