import axios from "axios";

export const GET_MESSAGES_REQUEST = 'GET_MESSAGES_REQUEST';
export const GET_MESSAGES_SUCCESS = 'GET_MESSAGES_SUCCESS';
export const GET_MESSAGES_FAILURE = 'GET_MESSAGES_FAILURE';
export const CHANGE_MODAL = 'CHANGE_MODAL';

export const getMessagesRequest = () => ({type: GET_MESSAGES_REQUEST});
export const getMessagesSuccess = messages => ({type: GET_MESSAGES_SUCCESS, payload: messages});
export const getMessagesFailure = error => ({type: GET_MESSAGES_FAILURE, payload: error});
export const changeModal = isOpen => ({type: CHANGE_MODAL, payload: isOpen});

export const getMessages = () => {
    return async dispatch => {
        try {
            dispatch(getMessagesRequest());

            //если использовать localhost:8008, то выходит ошибка связанная с CORS. app.use(cors()); использовал
            const response = await axios.get('http://127.0.0.1:8888/messages');
            dispatch(getMessagesSuccess(response.data));
        } catch (error) {
            dispatch(getMessagesFailure(error));
        }
    };
};