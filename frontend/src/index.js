import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import messagesReducer from "./store/reducer/messagesReducer";
import sendMessageReducer from "./store/reducer/sendMessageReducer";
import {CssBaseline} from "@material-ui/core";

const rootReducer = combineReducers({
    messages: messagesReducer,
    sendMessage: sendMessageReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <Provider store={store}>
        <CssBaseline/>
        <App/>
    </Provider>
);


ReactDOM.render(app, document.getElementById('root'));

