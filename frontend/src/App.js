import Layout from "./components/Layout/Layout";
import {Container} from "@material-ui/core";
import Messages from "./components/Messages/Messages";
import FormModal from "./components/FormModal/FormModal";

const App = () => {
    return (
        <Layout>
            <Container>
                <FormModal/>
                <Messages/>
            </Container>
        </Layout>
    );
};

export default App;
