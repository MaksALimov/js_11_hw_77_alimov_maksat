import React from 'react';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    background: {
        backgroundColor: 'gray',
        padding: '40px'
    }
}));

const Layout = ({children}) => {
    const classes = useStyles();
    return (
        <div className={classes.background}>
            {children}
        </div>
    );
};

export default Layout;