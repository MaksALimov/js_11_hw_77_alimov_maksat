import React, {useState} from 'react';
import {Button, createTheme, Grid, makeStyles, Modal, MuiThemeProvider, TextField} from "@material-ui/core";
import CancelSharpIcon from "@material-ui/icons/CancelSharp";
import SendIcon from "@material-ui/icons/Send";
import {useDispatch, useSelector} from "react-redux";
import {changeModal} from "../../store/actions/messagesActions";
import {sendMessage} from "../../store/actions/sendMessageActions";

const theme = createTheme({
    palette: {
        primary: {
            main: '#fff',
        },
    },
});

const useStyles = makeStyles(() => ({
    form: {
        margin: '100px 50px',
    },

    modal: {
        backgroundColor: 'white',
    },

    textFields: {
        marginBottom: '30px',
    },

    addMessageButton: {
        display: 'block',
        margin: '50px auto 0',
    },

    buttons: {
        margin: '0 20px',
    },

    input: {
        color: '#fff;',
        fontWeight: 'bold',
    }
}));

const SendMessage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [message, setMessage] = useState({
        author: '',
        message: '',
        image: null,
    });

    const modal = useSelector(state => state.messages.isOpenModal);
    const error = useSelector(state => state.sendMessage.error);

    const openModal = boolean => {
        dispatch(changeModal(boolean));
    };

    const submitFormHandler = async e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(message).forEach(key => {
            formData.append(key, message[key]);
        });

        await dispatch(sendMessage(formData));
        setMessage(prevState => ({...prevState, author: '', message: '', image: null}));
        openModal(false);
    };

    const onInputChangeHandler = e => {
        const {name, value} = e.target;
        setMessage(prevState => ({...prevState, [name]: value}));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setMessage(prevState => ({...prevState, [name]: file}));
    };

    return (
        <>
            <Button
                className={classes.addMessageButton}
                onClick={() => openModal(true)}
                variant="contained"
            >
                Add message
            </Button>
            <Modal
                className={classes.modal}
                open={modal}
            >
                <form className={classes.form} onSubmit={submitFormHandler}>
                    <Grid container direction="column" alignItems="center">
                        <MuiThemeProvider theme={theme}>
                            <TextField
                                label="Enter your name"
                                variant="outlined"
                                name="author"
                                value={message.author}
                                onChange={onInputChangeHandler}
                                className={classes.textFields}
                                inputProps={{
                                    className: classes.input
                                }}
                            />
                            <TextField
                                label="Enter message"
                                variant="outlined"
                                name="message"
                                error={!!error}
                                value={message.message}
                                multiline
                                onChange={onInputChangeHandler}
                                className={classes.textFields}
                                inputProps={{
                                    className: classes.input
                                }}
                            />
                            <TextField
                                type="file"
                                name="image"
                                variant="outlined"
                                onChange={fileChangeHandler}
                                className={classes.textFields}
                            />
                            <Grid item xs={12} md={12} sm={12}>
                                <Button
                                    onClick={() => openModal(false)}
                                    variant="contained"
                                    className={classes.buttons}
                                    startIcon={<CancelSharpIcon/>}
                                >
                                    Close
                                </Button>
                                <Button
                                    variant="contained"
                                    className={classes.buttons}
                                    startIcon={<SendIcon/>}
                                    type="submit"
                                >
                                    Send
                                </Button>
                            </Grid>
                        </MuiThemeProvider>
                    </Grid>
                </form>
            </Modal>
        </>
    );
};

export default SendMessage;