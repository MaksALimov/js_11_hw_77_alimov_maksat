import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {apiUrl} from "../../../congig";

const useStyles = makeStyles(() => ({
    messagesInsideContainer: {
        backgroundColor: 'rgba(255, 255, 255, .2)',
        borderRadius: '20px',
        padding: '20px',
        marginBottom: '30px',
    },

    test: {
        minHeight: '300px',
    },

    author: {
        textTransform: 'capitalize',
    },

    textColor: {
        color: '#fff',
        fontWeight: 'bold',
        marginBottom: '20px',
        marginRight: '20px',
        maxWidth: '1100px',
        wordWrap: "break-word"
    },
    dateTime: {
        color: 'LightGray'
    },

    image: {
        maxWidth: '100%',
        maxHeight: '100%',
    }
}))

const Message = ({author, datetime, message, image}) => {
    const classes = useStyles();
    const imageUrl = apiUrl + '/uploads/' + image;

    return (
        <Grid container className={classes.messagesInsideContainer}>
            <Typography className={`${classes.textColor} ${classes.author}`}>{author}</Typography>
            <Typography className={classes.dateTime}>{datetime}</Typography>
            <Grid item container>
                <Typography className={classes.textColor}>{message}</Typography>
                {image ? <img src={imageUrl} className={classes.image} alt="somePhoto"/> : null}
            </Grid>
        </Grid>
    );
};

export default Message;