import React, {useEffect} from 'react';
import {Grid, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getMessages} from "../../store/actions/messagesActions";
import Message from "./Message/Message";

const useStyles = makeStyles(() => ({
    background: {
        backgroundColor: '#000',
        borderRadius: '30px',
        padding: '30px',
        marginTop: '200px',
    },
}));

const Messages = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getMessages());
    }, [dispatch]);

    const messages = useSelector(state => state.messages.messages);

    return (
        <Grid container direction="column" className={classes.background}>
            {messages.map(message => (
                <Message
                    key={message.id}
                    message={message.message}
                    author={message.author}
                    datetime={message.datetime}
                    image={message.image}
                />
            ))}
        </Grid>
    );
};

export default Messages;