const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const messagesDb = require('../messagesDb');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    return res.send(messagesDb.getMessages());
});

router.post('/', upload.single('image'), (req, res) => {
    if (!req.body.message) {
        return res.status(400).send({error: 'Message must not be empty'});
    }

    const message = {
        author: req.body.author || 'Anonymous',
        message: req.body.message,
    };

    if (req.file) {
        message.image = req.file.filename;
    }

    const newMessage = messagesDb.addMessage(message);

    return res.send(newMessage);
});

module.exports = router;