const express = require('express');
const cors = require('cors');
const messages = require('./app/messages');
const messagesDb = require('./messagesDb');
const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8888;
app.use('/messages', messages);

messagesDb.init();
app.listen(port, () => {
   console.log(`Server started on ${port} port!`);
});