const fs = require('fs');
const dayjs = require('dayjs');
const {nanoid} = require('nanoid');

const fileName = './db.json';
let messages = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(fileName);
            messages = JSON.parse(fileContents);
        } catch (error) {
            messages = [];
        }
    },

    getMessages() {
        return messages;
    },

    addMessage(message) {
        message.id = nanoid();
        message.datetime = dayjs().format('MMM DD/MM/YYYY HH:mm:ss/ddd');
        messages.push(message);
        this.save();
        return message;
    },

    save () {
        fs.writeFileSync(fileName, JSON.stringify(messages));
    }
}